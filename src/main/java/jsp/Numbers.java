package jsp;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/numbers")
public class Numbers extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Peaks panema andmed siia sisse ja siis forwardima selle jsp'le mis
		// selle v�lja n�itab

		request.setAttribute("options", getOptionsMap());
		String listName = (String) request.getSession().getAttribute("listName");
		
		if ("odd".equals(listName)) {
			request.setAttribute("list", getOddMap());
		}else if ("even".equals(listName)) {
			request.setAttribute("list", getEvenMap());
		}

		request.getRequestDispatcher("WEB-INF/jsp/numbers.jsp").forward(request, response);
	}

	// POST v�tab vastu, paneb sessiooni ja suunab get'i
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// esimene on attribuuti nimi ja teine on v��rtus (objekt)
		request.getSession().setAttribute("listName", request.getParameter("listName"));
		
		response.sendRedirect("numbers");

	}
	
	private LinkedHashMap<String, String> getOptionsMap() {
		LinkedHashMap<String, String> optionsMap = new LinkedHashMap<>();
		optionsMap.put("", "");
		optionsMap.put("odd", "Odd");
		optionsMap.put("even", "Even");
		return optionsMap;
	}

	private LinkedHashMap<String, String> getEvenMap() {
		LinkedHashMap<String, String> evenMap = new LinkedHashMap<String, String>();
		evenMap.put("1", "kaks");
		evenMap.put("2", "neli");
		evenMap.put("3", "kuus");
		evenMap.put("4", "kaheksa");
		return evenMap;
	}

	private LinkedHashMap<String, String> getOddMap() {
		LinkedHashMap<String, String> oddMap = new LinkedHashMap<String, String>();
		oddMap.put("1", "�ks");
		oddMap.put("2", "kolm");
		oddMap.put("3", "viis");
		oddMap.put("4", "seitse");
		return oddMap;
	}

}
