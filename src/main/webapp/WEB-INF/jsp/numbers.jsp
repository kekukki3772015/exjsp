<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.LinkedHashMap" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css">.even { color: red; } </style>
</head>
<body>

<%
	//pageContext on ainult jsp asi, Javas sellist asja ei ole
 	//pageContext.setAttribute("even", evenMap);
%>
<!-- listName pannakse parameetrina sisse: <c:out value="${param}"/> -->
<form method="post" action="numbers">
  <select name="listName">  
    <!-- ${pageScope['options']} kasutad siis kui andmed on jsp'le teada, 
    requestScope siis kui andmed tulevad requestist -->
    <c:forEach var="option" items="${requestScope['options']}">
    	<c:set var="selected" value=""/>
    	<c:if test="${option.key == sessionScope['listName']}">
    		<c:set var="selected" value="selected"/>
    	</c:if>
    	<option ${selected} value="${option.key}">${option.value}</option>
    </c:forEach>
  </select>

  <input type="submit" value="Värskenda">
</form>


<!-- Näidata valitud listi sisu välja -->
<!-- request'st otsida list ise ja sessioonist otsida millise listiga tegu (mis nimelise listiga) -->
<c:forEach var="entry" items="${requestScope.list}" varStatus="status">
	<c:set var="cssClass" value=""/>
	<c:if test="${status.count % 2 == 0}">
		<c:set var="cssClass" value="even"/>
	</c:if>
	<div class="${cssClass}">
		<c:out value="${entry.key}" />. <c:out value="${entry.value}" />
	</div>
</c:forEach>

</body>
</html>